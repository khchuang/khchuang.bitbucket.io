var searchData=
[
  ['l_33',['l',['../classtch__driver_1_1touchscreen.html#ab6fc8d982d82be3d7c50f13734615067',1,'tch_driver::touchscreen']]],
  ['lab3_5fmain_2epy_34',['Lab3_main.py',['../Lab3__main_8py.html',1,'']]],
  ['lab4_5fmain_2epy_35',['Lab4_main.py',['../Lab4__main_8py.html',1,'']]],
  ['lower_5flim_36',['lower_lim',['../classmcp9808_1_1MCP9808.html#abf2c91841f12d03840da16f181ee54ee',1,'mcp9808::MCP9808']]],
  ['lab0x03_3a_20pushing_20the_20right_20buttons_20documentation_37',['Lab0x03: Pushing the Right Buttons Documentation',['../page_button.html',1,'']]],
  ['lab0x05_3a_20feeling_20tipsy_3f_20documentation_38',['Lab0x05: Feeling Tipsy? Documentation',['../page_calc.html',1,'']]],
  ['lab0x04_3a_20hot_20or_20not_3f_20documentation_39',['Lab0x04: Hot or Not? Documentation',['../page_hot.html',1,'']]],
  ['lab0x06_3a_20simulation_20or_20reality_3f_40',['Lab0x06: Simulation or Reality?',['../page_sim.html',1,'']]],
  ['lab0x02_3a_20think_20fast_21_20documentation_41',['Lab0x02: Think Fast! Documentation',['../page_speed.html',1,'']]],
  ['lab0x07_3a_20feeling_20touchy_42',['Lab0x07: Feeling Touchy',['../page_tch.html',1,'']]],
  ['lab0x08_3a_20term_20project_20part_201_43',['Lab0x08: Term Project Part 1',['../page_TP1.html',1,'']]],
  ['lab0x09_3a_20term_20project_20part_202_44',['Lab0x09: Term Project Part 2',['../page_TP2.html',1,'']]],
  ['lab0x01_3a_20vendotron_20simulation_20documentation_45',['Lab0x01: Vendotron Simulation Documentation',['../page_vend.html',1,'']]]
];
