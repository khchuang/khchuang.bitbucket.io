/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Kyle Chuang | ME405 Mechatronics Portfolio", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Lab0x01: Vendotron Simulation Documentation", "page_vend.html", [
      [ "Summary", "page_vend.html#sec_sum1", null ],
      [ "State Transition Diagram", "page_vend.html#sec_transition1", null ]
    ] ],
    [ "Lab0x02: Think Fast! Documentation", "page_speed.html", [
      [ "Summary", "page_speed.html#sec_sum2", null ]
    ] ],
    [ "Lab0x03: Pushing the Right Buttons Documentation", "page_button.html", [
      [ "Summary", "page_button.html#sec_sum3", null ],
      [ "Task Diagram", "page_button.html#sec_transition3", null ]
    ] ],
    [ "Lab0x04: Hot or Not? Documentation", "page_hot.html", [
      [ "Summary", "page_hot.html#sec_sum4", null ],
      [ "Results from 8 hours of data collection", "page_hot.html#sec_results", null ]
    ] ],
    [ "Lab0x05: Feeling Tipsy? Documentation", "page_calc.html", [
      [ "Summary", "page_calc.html#sec_sum5", null ],
      [ "Givens and Assumptions", "page_calc.html#sec_assump", null ],
      [ "Modeling Process", "page_calc.html#sec_solve", null ]
    ] ],
    [ "Lab0x06: Simulation or Reality?", "page_sim.html", [
      [ "Summary", "page_sim.html#sec_sum6", null ],
      [ "Open Loop Simulation Results", "page_sim.html#sec_open", null ],
      [ "Closed Loop Simulation Results", "page_sim.html#sec_closed", null ]
    ] ],
    [ "Lab0x07: Feeling Touchy", "page_tch.html", [
      [ "Summary", "page_tch.html#sec_sum7", null ],
      [ "Hardware Setup", "page_tch.html#sec_hw", null ],
      [ "Code Testing and Verification", "page_tch.html#sec_verify1", null ],
      [ "Response Time Verification", "page_tch.html#sec_verify2", null ]
    ] ],
    [ "Lab0x08: Term Project Part 1", "page_TP1.html", [
      [ "Summary", "page_TP1.html#sec_sum8", null ]
    ] ],
    [ "Lab0x09: Term Project Part 2", "page_TP2.html", [
      [ "Summary", "page_TP2.html#sec_sum9", null ],
      [ "METHOD 1: Analytical Solution", "page_TP2.html#sec_m1", null ],
      [ "METHOD 2: LQR MATLAB Simulation Solution", "page_TP2.html#sec_m2", null ],
      [ "METHOD 3: Hardware Implementation", "page_TP2.html#sec_m3", null ],
      [ "Hardware Implementation: Kyle's Experience", "page_TP2.html#sec_kyle", null ],
      [ "Hardware Implementation: Enoch's Experience", "page_TP2.html#sec_enoch", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"Lab3__main_8py.html",
"page_sim.html#sec_sum6"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';