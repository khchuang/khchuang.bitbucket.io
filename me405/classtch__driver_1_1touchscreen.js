var classtch__driver_1_1touchscreen =
[
    [ "__init__", "classtch__driver_1_1touchscreen.html#a6baaf001b07aa244c7387dda299cf5b5", null ],
    [ "coord", "classtch__driver_1_1touchscreen.html#a2212bc079379c737f5af06758daf70be", null ],
    [ "Xscan", "classtch__driver_1_1touchscreen.html#abd64f2052457444439e44bf6461bfe3e", null ],
    [ "Yscan", "classtch__driver_1_1touchscreen.html#aba27a0cd92e4ba77f1b9f193766deda4", null ],
    [ "Zscan", "classtch__driver_1_1touchscreen.html#a34f389d2cd51e5e64ec8ac4364c057bb", null ],
    [ "ADC_ym", "classtch__driver_1_1touchscreen.html#a49687d21c08525417548d539bcc53946", null ],
    [ "l", "classtch__driver_1_1touchscreen.html#ab6fc8d982d82be3d7c50f13734615067", null ],
    [ "w", "classtch__driver_1_1touchscreen.html#a7c02b9391737505f7084609fde56a839", null ],
    [ "xc", "classtch__driver_1_1touchscreen.html#a398c5b13276cdcdfbff15be4bb6545ca", null ],
    [ "Xi", "classtch__driver_1_1touchscreen.html#a381d1253dfdc1dd81ddad7e0965e5593", null ],
    [ "xm", "classtch__driver_1_1touchscreen.html#ae9e0d2556f8698795d60bbfc1baaef92", null ],
    [ "xp", "classtch__driver_1_1touchscreen.html#afc71705bee08aa07c8adb79d6c0ad43e", null ],
    [ "yc", "classtch__driver_1_1touchscreen.html#a344c45f643f2e10676aab4d0305b6ec5", null ],
    [ "Yi", "classtch__driver_1_1touchscreen.html#a63b5ed566ecf91cc1cee5ff6c68e83b7", null ],
    [ "ym", "classtch__driver_1_1touchscreen.html#ad3ec5be9d03409ba45314c99a8b5acbd", null ],
    [ "yp", "classtch__driver_1_1touchscreen.html#acd082e2b6b570eb78a13966c27c365b5", null ]
];