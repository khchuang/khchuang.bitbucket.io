var annotated_dup =
[
    [ "BT_driver", null, [
      [ "BT", "classBT__driver_1_1BT.html", "classBT__driver_1_1BT" ]
    ] ],
    [ "c_back", null, [
      [ "TaskData", "classc__back_1_1TaskData.html", "classc__back_1_1TaskData" ]
    ] ],
    [ "c_back7", null, [
      [ "TaskData", "classc__back7_1_1TaskData.html", "classc__back7_1_1TaskData" ]
    ] ],
    [ "c_front", null, [
      [ "UI", "classc__front_1_1UI.html", "classc__front_1_1UI" ]
    ] ],
    [ "c_front7", null, [
      [ "UI", "classc__front7_1_1UI.html", "classc__front7_1_1UI" ]
    ] ],
    [ "ControllerClass", null, [
      [ "ClosedLoop", "classControllerClass_1_1ClosedLoop.html", "classControllerClass_1_1ClosedLoop" ]
    ] ],
    [ "ControllerTask", null, [
      [ "TaskControl", "classControllerTask_1_1TaskControl.html", "classControllerTask_1_1TaskControl" ]
    ] ],
    [ "ControllerTask7", null, [
      [ "TaskControl", "classControllerTask7_1_1TaskControl.html", "classControllerTask7_1_1TaskControl" ]
    ] ],
    [ "elevator", null, [
      [ "Button", "classelevator_1_1Button.html", "classelevator_1_1Button" ],
      [ "Elevator", "classelevator_1_1Elevator.html", "classelevator_1_1Elevator" ],
      [ "MotorDriver", "classelevator_1_1MotorDriver.html", "classelevator_1_1MotorDriver" ]
    ] ],
    [ "enc", null, [
      [ "EncoderDriver", "classenc_1_1EncoderDriver.html", "classenc_1_1EncoderDriver" ]
    ] ],
    [ "FSM_blink", null, [
      [ "BTblink", "classFSM__blink_1_1BTblink.html", "classFSM__blink_1_1BTblink" ]
    ] ],
    [ "FSM_enc", null, [
      [ "TaskEnc", "classFSM__enc_1_1TaskEnc.html", "classFSM__enc_1_1TaskEnc" ]
    ] ],
    [ "FSM_UI", null, [
      [ "TaskUI", "classFSM__UI_1_1TaskUI.html", "classFSM__UI_1_1TaskUI" ]
    ] ],
    [ "LED", null, [
      [ "TaskBlink", "classLED_1_1TaskBlink.html", "classLED_1_1TaskBlink" ]
    ] ],
    [ "LED_Pyth", null, [
      [ "TaskBlink", "classLED__Pyth_1_1TaskBlink.html", "classLED__Pyth_1_1TaskBlink" ]
    ] ],
    [ "m_driver", null, [
      [ "MotorDriver", "classm__driver_1_1MotorDriver.html", "classm__driver_1_1MotorDriver" ]
    ] ],
    [ "UI_dataGen", null, [
      [ "TaskData", "classUI__dataGen_1_1TaskData.html", "classUI__dataGen_1_1TaskData" ]
    ] ]
];