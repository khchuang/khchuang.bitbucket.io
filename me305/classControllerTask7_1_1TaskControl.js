var classControllerTask7_1_1TaskControl =
[
    [ "__init__", "classControllerTask7_1_1TaskControl.html#ae4e1040d0f2dc6423b85b31408d15c43", null ],
    [ "run", "classControllerTask7_1_1TaskControl.html#aab8ad79febf9ce40df9f4f36d81d968a", null ],
    [ "transitionTo", "classControllerTask7_1_1TaskControl.html#a1c2b5fe3beb08eeacf79312522d23f8d", null ],
    [ "Controller", "classControllerTask7_1_1TaskControl.html#ac370c6ee9df5d89da897682037673ee8", null ],
    [ "curr_time", "classControllerTask7_1_1TaskControl.html#a9911d5331ea940169903207f6d8257cf", null ],
    [ "Encoder", "classControllerTask7_1_1TaskControl.html#acd96e771ab148de3ed7583cd4c7dfa5d", null ],
    [ "interval", "classControllerTask7_1_1TaskControl.html#ab37d4dcbf796daf023df4e74a4e5b095", null ],
    [ "Motor", "classControllerTask7_1_1TaskControl.html#a9bb8ddefbb37d601d2ecf0d68f2daa96", null ],
    [ "next_time", "classControllerTask7_1_1TaskControl.html#a8c93c6c285871b2d00f8e572b48d44e6", null ],
    [ "omega_ref", "classControllerTask7_1_1TaskControl.html#af44262278915f44f4acde8fc9e9e57ae", null ],
    [ "runs", "classControllerTask7_1_1TaskControl.html#aca97d2ee66c5e1876ec1d4f400a8c288", null ],
    [ "start_time", "classControllerTask7_1_1TaskControl.html#adbc89e756a2ae890c4bd6e6d450dc918", null ],
    [ "state", "classControllerTask7_1_1TaskControl.html#af0b16b19372b6c7d6bddddc275e3fe06", null ]
];