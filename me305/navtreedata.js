/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Kyle Chuang | Portfolio", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Fibonacci Sequence", "page_fib.html", [
      [ "Summary", "page_fib.html#sec_sum1", null ]
    ] ],
    [ "Finite State Machine: Two-Story Elevator Simulation", "page_elv.html", [
      [ "Summary", "page_elv.html#sec_sum", null ],
      [ "State Transition Diagram", "page_elv.html#sec_FSM", null ]
    ] ],
    [ "Finite State Machine: Dual and Virtual LEDs", "page_LED.html", [
      [ "Summary", "page_LED.html#sec_sum2", null ],
      [ "State Transition Diagram", "page_LED.html#sec_FSM2", null ]
    ] ],
    [ "Encoder Finite State Machine", "page_enc.html", [
      [ "Summary", "page_enc.html#sec_sum3", null ],
      [ "Encoder State Transition Diagram", "page_enc.html#sec_FSM3", null ],
      [ "User Interface State Transition Diagram", "page_enc.html#sec_FSM4", null ]
    ] ],
    [ "Encoder Data Collection", "page_data.html", [
      [ "Summary", "page_data.html#sec_sum4", null ],
      [ "Encoder Data Collection Task Diagram", "page_data.html#sec_Task1", null ],
      [ "Data Collection State Transition Diagram", "page_data.html#sec_FSM5", null ]
    ] ],
    [ "Bluetooth Interfacing and App Creation", "page_blink.html", [
      [ "Summary", "page_blink.html#sec_sum5", null ],
      [ "Bluetooth Interface Task Diagram", "page_blink.html#sec_Task2", null ],
      [ "LED Blink Frequency Transition Diagram", "page_blink.html#sec_FSM6", null ],
      [ "Phone App UI", "page_blink.html#sec_pic1", null ]
    ] ],
    [ "Motor Driver", "page_motor.html", [
      [ "Summary", "page_motor.html#sec_sum6", null ],
      [ "Closed Loop Controller Task Diagram", "page_motor.html#sec_Task3", null ],
      [ "Front End Task Diagram", "page_motor.html#sec_FSM7", null ],
      [ "Back End Task Diagram", "page_motor.html#sec_FSM8", null ],
      [ "Close Loop Controller Task Diagram", "page_motor.html#sec_FSM9", null ],
      [ "Results", "page_motor.html#sec_res", null ],
      [ "Test 1: KP = 1", "page_motor.html#sec_test1", null ],
      [ "Test 2: KP = 0.1", "page_motor.html#sec_test2", null ],
      [ "Test 3: KP = 0.01", "page_motor.html#sec_test3", null ],
      [ "Test 4: KP = 0.001", "page_motor.html#sec_test4", null ]
    ] ],
    [ "Reference Tracking", "page_ref.html", [
      [ "Summary", "page_ref.html#sec_sum7", null ],
      [ "Results", "page_ref.html#sec_res1", null ],
      [ "Test 1: KP = 0.1", "page_ref.html#sec_t1", null ],
      [ "Test 2: KP = 0.5", "page_ref.html#sec_t2", null ],
      [ "Test 3: KP = 0.8", "page_ref.html#sec_t3", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"",
"classelevator_1_1Button.html#aa8c630539af0fdd86cade8e4df155757"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';