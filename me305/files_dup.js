var files_dup =
[
    [ "BT_driver.py", "BT__driver_8py.html", [
      [ "BT", "classBT__driver_1_1BT.html", "classBT__driver_1_1BT" ]
    ] ],
    [ "c_back.py", "c__back_8py.html", [
      [ "TaskData", "classc__back_1_1TaskData.html", "classc__back_1_1TaskData" ]
    ] ],
    [ "c_back7.py", "c__back7_8py.html", [
      [ "TaskData", "classc__back7_1_1TaskData.html", "classc__back7_1_1TaskData" ]
    ] ],
    [ "c_front.py", "c__front_8py.html", "c__front_8py" ],
    [ "c_front7.py", "c__front7_8py.html", "c__front7_8py" ],
    [ "ControllerClass.py", "ControllerClass_8py.html", [
      [ "ClosedLoop", "classControllerClass_1_1ClosedLoop.html", "classControllerClass_1_1ClosedLoop" ]
    ] ],
    [ "ControllerTask.py", "ControllerTask_8py.html", [
      [ "TaskControl", "classControllerTask_1_1TaskControl.html", "classControllerTask_1_1TaskControl" ]
    ] ],
    [ "ControllerTask7.py", "ControllerTask7_8py.html", [
      [ "TaskControl", "classControllerTask7_1_1TaskControl.html", "classControllerTask7_1_1TaskControl" ]
    ] ],
    [ "elevator.py", "elevator_8py.html", [
      [ "Elevator", "classelevator_1_1Elevator.html", "classelevator_1_1Elevator" ],
      [ "Button", "classelevator_1_1Button.html", "classelevator_1_1Button" ],
      [ "MotorDriver", "classelevator_1_1MotorDriver.html", "classelevator_1_1MotorDriver" ]
    ] ],
    [ "enc.py", "enc_8py.html", [
      [ "EncoderDriver", "classenc_1_1EncoderDriver.html", "classenc_1_1EncoderDriver" ]
    ] ],
    [ "fib.py", "fib_8py.html", "fib_8py" ],
    [ "FSM_blink.py", "FSM__blink_8py.html", "FSM__blink_8py" ],
    [ "FSM_enc.py", "FSM__enc_8py.html", [
      [ "TaskEnc", "classFSM__enc_1_1TaskEnc.html", "classFSM__enc_1_1TaskEnc" ]
    ] ],
    [ "FSM_UI.py", "FSM__UI_8py.html", [
      [ "TaskUI", "classFSM__UI_1_1TaskUI.html", "classFSM__UI_1_1TaskUI" ]
    ] ],
    [ "LED.py", "LED_8py.html", [
      [ "TaskBlink", "classLED_1_1TaskBlink.html", "classLED_1_1TaskBlink" ]
    ] ],
    [ "LED_Pyth.py", "LED__Pyth_8py.html", [
      [ "TaskBlink", "classLED__Pyth_1_1TaskBlink.html", "classLED__Pyth_1_1TaskBlink" ]
    ] ],
    [ "m_driver.py", "m__driver_8py.html", [
      [ "MotorDriver", "classm__driver_1_1MotorDriver.html", "classm__driver_1_1MotorDriver" ]
    ] ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "main_blink.py", "main__blink_8py.html", "main__blink_8py" ],
    [ "main_elev.py", "main__elev_8py.html", "main__elev_8py" ],
    [ "main_enc.py", "main__enc_8py.html", "main__enc_8py" ],
    [ "shares.py", "shares_8py.html", "shares_8py" ],
    [ "UI_dataGen.py", "UI__dataGen_8py.html", [
      [ "TaskData", "classUI__dataGen_1_1TaskData.html", "classUI__dataGen_1_1TaskData" ]
    ] ],
    [ "UI_front.py", "UI__front_8py.html", "UI__front_8py" ]
];