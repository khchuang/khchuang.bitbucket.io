var classControllerClass_1_1ClosedLoop =
[
    [ "__init__", "classControllerClass_1_1ClosedLoop.html#a8dc3e17e11ce6d7a7a77208f74e5c03a", null ],
    [ "get_Kp", "classControllerClass_1_1ClosedLoop.html#a7467ed301371fd1adfaffcb05560f64d", null ],
    [ "set_Kp", "classControllerClass_1_1ClosedLoop.html#af7c798be11d42dcdc352b45dfb9a8e1f", null ],
    [ "update", "classControllerClass_1_1ClosedLoop.html#ac93cb6699b9b60d2bae521cd7bf00026", null ],
    [ "Kp", "classControllerClass_1_1ClosedLoop.html#a817a3f2dd9f4c545abd4503a6486dd43", null ],
    [ "Kp_prime", "classControllerClass_1_1ClosedLoop.html#aac7af4825d2d1179f80d52fce994442f", null ],
    [ "L_new", "classControllerClass_1_1ClosedLoop.html#aec49251286682bf92ecc12f7846a0e42", null ],
    [ "L_old", "classControllerClass_1_1ClosedLoop.html#a36b9f7c6cca19bb11a341f19633f27b2", null ],
    [ "VDC", "classControllerClass_1_1ClosedLoop.html#a788b8f68e092573ba525060b8753d2bb", null ],
    [ "w", "classControllerClass_1_1ClosedLoop.html#a3647ef7e1d179866734066d32b0fb24b", null ],
    [ "w_ref", "classControllerClass_1_1ClosedLoop.html#a88bd6c677ab1ff76bb1c74edebea3dcd", null ]
];